import React, { useState } from 'react';
import './index.scss';
import { NavLink } from 'react-router-dom';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';

const menuList = [
  {
    id: "dashboard",
    name: "Dashboard",
    links: '/dashboard'
  },
  {
    id: "customers",
    name: "Customers",
    links: '/customers'
  },
  {
    id: "tasks",
    name: "Tasks",
    links: '/tasks'
  },
  {
    id: "orders",
    name: "Orders",
    links: '/orders'
  }
];

const Menus = () => {
  const [characters, updateCharacters] = useState(menuList);

  function handleOnDragEnd(result) {
    if (!result.destination) return;

    const items = Array.from(characters);
    const [reorderedItem] = items.splice(result.source.index, 1);
    items.splice(result.destination.index, 0, reorderedItem);

    updateCharacters(items);
  }
  return (
    <DragDropContext onDragEnd={handleOnDragEnd}>
      <Droppable droppableId="characters">
        {(provided) => (
          <ul className="characters" {...provided.droppableProps} ref={provided.innerRef}>
            {characters.map(({ id, name, links }, index) => {
              return (
                <Draggable key={id} draggableId={id} index={index}>
                  {(provided) => (
                    <li ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps}>
                      <NavLink to={links}>{name}</NavLink>
                    </li>
                  )}
                </Draggable>
              );
            })}
            {provided.placeholder}
          </ul>
        )}
      </Droppable>
    </DragDropContext>
  )
}

export default Menus;