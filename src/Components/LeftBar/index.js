import React, { useState } from 'react';
import MediaQuery from 'react-responsive';
import Menus from '../Menus';
import { Button, Drawer } from 'antd';
import { Link } from 'react-router-dom';
import './index.scss';

import logolight from '../../assets/images/logo-light.svg';
import SvgIcon from '../svg-icon/svg-icon';

const LeftBar = () => {
    const [openMenu, setOpenMenu] = useState(false);
    const showDrawer = () => {
        setOpenMenu(true);
    };

    const onCloseDrawer = () => {
        setOpenMenu(false);
    };
    return (
        <>
            <MediaQuery minWidth={991}>
                <div className='leftbar'>
                    <div className='logo'>
                        <img className='logo-light' src={logolight} alt='' />
                    </div>
                    <Menus />
                    <Link className='logout-btn' to='/'><SvgIcon name='logout' viewbox="0 0 321 290" /> Logout</Link>
                </div>
            </MediaQuery>
            <MediaQuery maxWidth={991}>
                <Button className='menu-btn' type="primary" onClick={showDrawer}>
                    <SvgIcon name="menu" viewbox="0 0 46 32" />
                </Button>
                <Drawer placement='left' width={280} className='menu-drawer' title={null} closeIcon={false} onClose={onCloseDrawer} open={openMenu}>
                    <div className='leftbar'>
                        <div className='logo'>
                            <img className='logo-light' src={logolight} alt='' />
                        </div>
                        <div>
                            <Menus />
                        </div>
                    </div>
                </Drawer>
            </MediaQuery>
        </>
    )
}

export default LeftBar;