import React, { useEffect, useState } from "react";
import { Button, Checkbox } from "antd";
import './index.scss';

let deferredPrompt;

function AppInstaller() {
  const [installable, setInstallable] = useState(false);
  const [showBanner, setShowBanner] = useState(null);

  useEffect(() => {
    // const isLocalStorage = "localStorage" in window && window.localStorage !== null;

    if (localStorage.getItem("showBanner") === null) {
      setShowBanner(true);
      localStorage.setItem("showBanner", true);
    } else {
      setShowBanner(
        JSON.parse(localStorage.getItem("showBanner").toLowerCase())
      );
    }

    window.addEventListener("beforeinstallprompt", (e) => {
      // Prevent the mini-infobar from appearing on mobile
      e.preventDefault();
      // Stash the event so it can be triggered later.
      deferredPrompt = e;
      // Update UI notify the user they can install the PWA
      setInstallable(true);
      // localStorage.setItem("showBanner", true);
    });

    window.addEventListener("appinstalled", () => {
      // Log install to analytics
      console.log("INSTALL: Success");
      setShowBanner(false);
      localStorage.setItem("showBanner", false);
    });
  }, [showBanner]);

  const handleCheckbox = (e) => {
    localStorage.setItem("showBanner", !e.target.checked);
  };

  const handleClose = (e) => {
    setInstallable(false);
    setShowBanner(false);
  };

  const handleInstallClick = (e) => {
    // Hide the app provided install promotion
    setInstallable(false);
    // Show the install prompt
    if (deferredPrompt) {
      deferredPrompt.prompt();
    }
    // Wait for the user to respond to the prompt
    deferredPrompt.userChoice.then((choiceResult) => {
      if (choiceResult.outcome === "accepted") {
        console.log("User accepted the install prompt");
      } else {
        console.log("User dismissed the install prompt");
      }
    });
  };

  return (
    <>
      {installable && showBanner && (
        <div className="app-install-banner">
          <div className="left-col">
            <p>
              Add <b>CRM</b> to your home screen
            </p>

            <Checkbox onChange={handleCheckbox}>
              Do not show this message again.
            </Checkbox>
          </div>
          <div className="pwa-button">
            <Button onClick={handleClose}>
              Close
            </Button>

            <Button
              onClick={handleInstallClick}
              className="install-button"
            >
              Install
            </Button>
          </div>
        </div>
      )}
    </>
  );
}

export default AppInstaller;
