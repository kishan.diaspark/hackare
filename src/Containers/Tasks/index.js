import React from 'react';
import SvgIcon from '../../Components/svg-icon/svg-icon';
import LeftBar from '../../Components/LeftBar';
import CrmChat from '../CrmChat';
import './index.scss';

const Tasks = () => {
    return (
        <div className='main-wrapper'>
            <CrmChat />
            <LeftBar />
            <div className='right-wrapper'>
                <div className='main-title'>Tasks List
                    <button className='main-btn'><SvgIcon name='plus' viewbox="0 0 46 46" /> add Tasks</button>
                </div>
                <div className='table-responsive'>
                    <table className='list-table'>
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Type</th>
                                <th>Email</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>001</td>
                                <td>kishan</td>
                                <td>Employee</td>
                                <td>kishan@gmail.com</td>
                                <td>
                                    <SvgIcon name='edit' viewbox="0 0 43 43" />
                                </td>
                            </tr>
                            <tr>
                                <td>002</td>
                                <td>Ishan</td>
                                <td>Employee</td>
                                <td>Ishan@gmail.com</td>
                                <td>
                                    <SvgIcon name='edit' viewbox="0 0 43 43" />
                                </td>
                            </tr>
                            <tr>
                                <td>003</td>
                                <td>TanuShri</td>
                                <td>Employee</td>
                                <td>TanuShri@gmail.com</td>
                                <td>
                                    <SvgIcon name='edit' viewbox="0 0 43 43" />
                                </td>
                            </tr>
                            <tr>
                                <td>004</td>
                                <td>Maynk</td>
                                <td>Employee</td>
                                <td>Maynk@gmail.com</td>
                                <td>
                                    <SvgIcon name='edit' viewbox="0 0 43 43" />
                                </td>
                            </tr>
                            <tr>
                                <td>005</td>
                                <td>Maynk</td>
                                <td>Employee</td>
                                <td>Maynk@gmail.com</td>
                                <td>
                                    <SvgIcon name='edit' viewbox="0 0 43 43" />
                                </td>
                            </tr>
                            <tr>
                                <td>006</td>
                                <td>Maynk</td>
                                <td>Employee</td>
                                <td>Maynk@gmail.com</td>
                                <td>
                                    <SvgIcon name='edit' viewbox="0 0 43 43" />
                                </td>
                            </tr>
                            <tr>
                                <td>007</td>
                                <td>Maynk</td>
                                <td>Employee</td>
                                <td>Maynk@gmail.com</td>
                                <td>
                                    <SvgIcon name='edit' viewbox="0 0 43 43" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    )
}

export default Tasks;