import React, { useState } from 'react';
import LeftBar from '../../Components/LeftBar';
import Lottie from 'react-lottie';
import CrmChat from '../CrmChat';
import { Button, Form, Input, Modal } from 'antd';
import { Link } from 'react-router-dom';
import './index.scss';

import animationData from '../../assets/images/success.json';

const CreateCustomer = () => {
    const [isModalOpen, setIsModalOpen] = useState(false);

    const showModal = () => {
        setIsModalOpen(true);
    };

    const handleOk = () => {
        setIsModalOpen(false);
    };

    const handleCancel = () => {
        setIsModalOpen(false);
    };

    const loginAnimation = {
        loop: true,
        autoplay: true,
        animationData: animationData,
        rendererSettings: {
            preserveAspectRatio: 'xMidYMid slice'
        }
    };
    return (
        <div className='main-wrapper'>
            <CrmChat />
            <LeftBar />
            <div className='right-wrapper'>
                <div className='main-title'>Create Customers</div>
                <Form
                    name="basic"
                    wrapperCol={{ span: 24 }}
                    style={{ maxWidth: 700 }}
                    initialValues={{ remember: true }}
                    autoComplete="off"
                    layout='vertical'
                    className='create-form'
                >
                    <Form.Item
                        label="First Name"
                        name="fname"
                    >
                        <Input size='large' />
                    </Form.Item>

                    <Form.Item
                        label="Last Name"
                        name="lname"
                    >
                        <Input size='large' />
                    </Form.Item>

                    <Form.Item
                        label="Phone Number"
                        name="phone"
                    >
                        <Input size='large' />
                    </Form.Item>

                    <Form.Item
                        label="Email"
                        name="email"
                    >
                        <Input size='large' />
                    </Form.Item>

                    <Form.Item
                        label="Address"
                        name="address"
                    >
                        <Input size='large' />
                    </Form.Item>

                    <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                        <Button size='large' type="primary" htmlType="submit" onClick={showModal}>
                            Submit
                        </Button>
                    </Form.Item>
                </Form>
                <Modal className='success-modal' closeIcon={false} centered footer={false} open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
                    <Lottie options={loginAnimation}
                        height={300}
                        width={300}
                    />
                    <div className='text-center'>
                        <h3>Customer Created Successfully !</h3>
                        <Link to='/customers'>
                            <Button size='large'>
                                OKay
                            </Button>
                        </Link>
                    </div>
                </Modal>
            </div>
        </div>
    )
}

export default CreateCustomer;