import React from 'react';
import SvgIcon from '../../Components/svg-icon/svg-icon';
import LeftBar from '../../Components/LeftBar';
import CrmChat from '../CrmChat';
import { Link } from 'react-router-dom';
import './index.scss';

const Customers = () => {
    return (
        <div className='main-wrapper'>
            <CrmChat />
            <LeftBar />
            <div className='right-wrapper'>
                <div className='main-title'>Customers List
                    <Link to='/create-customer'><button className='main-btn'><SvgIcon name='plus' viewbox="0 0 46 46" /> add Customer</button></Link>
                </div>
                <div className='table-responsive'>
                    <table className='list-table'>
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>001</td>
                                <td>Tanu Shri</td>
                                <td>9865986598</td>
                                <td>Tanushri@gmail.com</td>
                                <td>
                                    <SvgIcon name='edit' viewbox="0 0 43 43" />
                                </td>
                            </tr>
                            <tr>
                                <td>002</td>
                                <td>Kishan</td>
                                <td>9865986598</td>
                                <td>Kishan@gmail.com</td>
                                <td>
                                    <SvgIcon name='edit' viewbox="0 0 43 43" />
                                </td>
                            </tr>
                            <tr>
                                <td>003</td>
                                <td>Ishan</td>
                                <td>9865986598</td>
                                <td>Ishan@gmail.com</td>
                                <td>
                                    <SvgIcon name='edit' viewbox="0 0 43 43" />
                                </td>
                            </tr>
                            <tr>
                                <td>004</td>
                                <td>Maynk</td>
                                <td>9865986598</td>
                                <td>Maynk@gmail.com</td>
                                <td>
                                    <SvgIcon name='edit' viewbox="0 0 43 43" />
                                </td>
                            </tr>
                            <tr>
                                <td>005</td>
                                <td>Chris</td>
                                <td>9865986598</td>
                                <td>qaz@gmail.com</td>
                                <td>
                                    <SvgIcon name='edit' viewbox="0 0 43 43" />
                                </td>
                            </tr>
                            <tr>
                                <td>005</td>
                                <td>Maynk</td>
                                <td>9865986598</td>
                                <td>xyz@gmail.com</td>
                                <td>
                                    <SvgIcon name='edit' viewbox="0 0 43 43" />
                                </td>
                            </tr>
                            <tr>
                                <td>005</td>
                                <td>Jack</td>
                                <td>9865986598</td>
                                <td>demo@gmail.com</td>
                                <td>
                                    <SvgIcon name='edit' viewbox="0 0 43 43" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    )
}

export default Customers;