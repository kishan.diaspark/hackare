import React from 'react';
import SvgIcon from '../../Components/svg-icon/svg-icon';
import LeftBar from '../../Components/LeftBar';
import './index.scss';

const Orders = () => {
    return (
        <div className='main-wrapper'>
            <LeftBar />
            <div className='right-wrapper'>
                <div className='main-title'>Orders List
                    <button className='main-btn'><SvgIcon name='plus' viewbox="0 0 46 46" /> Add Orders</button>
                </div>
                <div className='table-responsive'>
                    <table className='list-table'>
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>001</td>
                                <td>Maynk</td>
                                <td>9865986598</td>
                                <td>Maynk@gmail.com</td>
                                <td>
                                    <SvgIcon name='edit' viewbox="0 0 43 43" />
                                </td>
                            </tr>
                            <tr>
                                <td>001</td>
                                <td>Maynk</td>
                                <td>9865986598</td>
                                <td>Maynk@gmail.com</td>
                                <td>
                                    <SvgIcon name='edit' viewbox="0 0 43 43" />
                                </td>
                            </tr>
                            <tr>
                                <td>001</td>
                                <td>Maynk</td>
                                <td>9865986598</td>
                                <td>Maynk@gmail.com</td>
                                <td>
                                    <SvgIcon name='edit' viewbox="0 0 43 43" />
                                </td>
                            </tr>
                            <tr>
                                <td>001</td>
                                <td>Maynk</td>
                                <td>9865986598</td>
                                <td>Maynk@gmail.com</td>
                                <td>
                                    <SvgIcon name='edit' viewbox="0 0 43 43" />
                                </td>
                            </tr>
                            <tr>
                                <td>001</td>
                                <td>Maynk</td>
                                <td>9865986598</td>
                                <td>Maynk@gmail.com</td>
                                <td>
                                    <SvgIcon name='edit' viewbox="0 0 43 43" />
                                </td>
                            </tr>
                            <tr>
                                <td>001</td>
                                <td>Maynk</td>
                                <td>9865986598</td>
                                <td>Maynk@gmail.com</td>
                                <td>
                                    <SvgIcon name='edit' viewbox="0 0 43 43" />
                                </td>
                            </tr>
                            <tr>
                                <td>001</td>
                                <td>Maynk</td>
                                <td>9865986598</td>
                                <td>Maynk@gmail.com</td>
                                <td>
                                    <SvgIcon name='edit' viewbox="0 0 43 43" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    )
}

export default Orders;