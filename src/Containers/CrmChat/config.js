import React from "react";
import { createChatBotMessage } from "react-chatbot-kit";

import ChatOptions from "./ChatOptions/ChatOptions";
import LinkList from "./LinkList/LinkList";

const config = {
    botName: "LuxareBot",
    initialMessages: [
        createChatBotMessage("Hi, I'm here to help. What do you want?", {
            widget: "chatOptions",
        }),
    ],
    customStyles: {
        botMessageBox: {
            backgroundColor: "#165141",
        },
        chatButton: {
            backgroundColor: "#376B7E",
        },
    },
    widgets: [
        {
            widgetName: "chatOptions",
            widgetFunc: (props) => <ChatOptions {...props} />,
        },
        {
            widgetName: "createForm",
            widgetFunc: (props) => <LinkList {...props} />,
            props: {
                options: [
                    {
                        text: "Crate Customer",
                        url: "/create-customer",
                        id: 1,
                    },
                ],
            },
        },
        {
            widgetName: "taskLinks",
            widgetFunc: (props) => <LinkList {...props} />,
            props: {
                options: [
                    {
                        text: "Logout",
                        url: "/",
                        id: 1,
                    },
                ],
            },
        },
        {
            widgetName: "CustomersPages",
            widgetFunc: (props) => <LinkList {...props} />,
            props: {
                options: [
                    {
                        text: "Customer List",
                        url: "/customers",
                        id: 1,
                    },
                    {
                        text: "Create Customer",
                        url: "/create-customer",
                        id: 2,
                    },
                ],
            },
        },
        {
            widgetName: "dashboardW",
            widgetFunc: (props) => <LinkList {...props} />,
            props: {
                options: [
                    {
                        text: "Dashboard",
                        url: "/dashboard",
                        id: 1,
                    },
                ],
            },
        },
    ],
};

export default config;