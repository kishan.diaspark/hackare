class ActionProvider {
  constructor(createChatBotMessage, setStateFunc) {
    this.createChatBotMessage = createChatBotMessage;
    this.setState = setStateFunc;
  }

  // new method
  greet() {
    const greetingMessage = this.createChatBotMessage("Hi, friend.");
    this.updateChatbotState(greetingMessage);
  }

  handlecreateForm = () => {
    const message = this.createChatBotMessage(
      "Fantastic, I've got some Create Form for now",
      {
        widget: "createForm",
      }
    );

    this.updateChatbotState(message);
  };

  handleCustomer = () => {
    const message = this.createChatBotMessage(
      "Check out Customer pages",
      {
        widget: "CustomersPages",
      }
    );

    this.updateChatbotState(message);
  };

  handleTask = () => {
    const message = this.createChatBotMessage(
      "We have Task List only, Click below to open task list",
      {
        widget: "taskLinks",
      }
    );

    this.updateChatbotState(message);
  };

  handleDashboard = () => {
    const message = this.createChatBotMessage(
      "Fantastic, Check Out Dashboard",
      {
        widget: "dashboardW",
      }
    );

    this.updateChatbotState(message);
  };

  updateChatbotState(message) {
    // NOTICE: This function is set in the constructor, and is passed in from the top level Chatbot component. The setState function here actually manipulates the top level state of the Chatbot, so it's important that we make sure that we preserve the previous state.

    this.setState((prevState) => ({
      ...prevState,
      messages: [...prevState.messages, message],
    }));
  }
}

export default ActionProvider;