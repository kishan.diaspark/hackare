import React from "react";

import "./ChatOptions.scss";

const ChatOptions = (props) => {
  const options = [
    {
      text: "Create",
      handler: props.actionProvider.handlecreateForm,
      id: 1,
    },
    { text: "Task", handler: props.actionProvider.handleTask, id: 2 },
    { text: "Customers", handler: props.actionProvider.handleCustomer, id: 3 },
    { text: "Dashboard", handler: props.actionProvider.handleDashboard, id: 4 },
  ];

  const optionsMarkup = options.map((option) => (
    <button
      className="learning-option-button"
      key={option.id}
      onClick={option.handler}
    >
      {option.text}
    </button>
  ));

  return <div className="learning-options-container">{optionsMarkup}</div>;
};

export default ChatOptions;