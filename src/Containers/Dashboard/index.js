import React, { useRef, useState, useEffect } from 'react';
import SvgIcon from '../../Components/svg-icon/svg-icon';
import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";
import { Button, Select, Tour, Drawer } from 'antd';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import Menus from '../../Components/Menus';
import MediaQuery from 'react-responsive';
import CrmChat from '../CrmChat';
import { Link } from 'react-router-dom';
import './index.scss';

import logolight from '../../assets/images/logo-light.svg';
import dargCardImg from '../../assets/images/card-tour.gif';
import dargMenuImg from '../../assets/images/menu-tour.gif';

const menuList = [
    {
        id: "totalCustomers",
        name: "Total Customers",
        icon: 'customers',
        viewbox: '0 0 138 169',
        counts: 800,
    },
    {
        id: "totaltaks",
        name: "Total Taks",
        icon: 'tasks',
        viewbox: '0 0 46 58',
        counts: 600,
    },
    {
        id: "totalorders",
        name: "Total Orders",
        icon: 'orders',
        viewbox: '0 0 187 187',
        counts: 2000,
    },
    {
        id: "revenue",
        name: "Revenue",
        icon: 'revenue',
        viewbox: '0 0 60 52',
        counts: "$2500",
    }
];

const chartData = {
    chart: {
        type: "column",
        spacingBottom: 0,
        spacingTop: 0,
        height: 320,
        backgroundColor: false,
        margin: [20, null, null, 50],
        title: null
    },
    title: {
        text: "",
    },
    xAxis: {
        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun'],
        title: {
            text: null
        },
        gridLineWidth: 0,
        lineWidth: 0
    },
    yAxis: {
        min: 0,
        labels: {
            overflow: 'justify',
            classNames: '00000000',
        },
        gridLineWidth: 0
    },
    tooltip: {
        valueSuffix: ''
    },
    plotOptions: {
        column: {
            borderRadius: '50%',
            borderWidth: 0,
            dataLabels: {
                enabled: false
            },
            groupPadding: 0.1
        }
    },
    legend: {
        enabled: false,
    },
    credits: {
        enabled: false
    },
    series: [{
        color: "#165141",
        name: 'Year 2023',
        data: [631, 727, 500, 400, 600, 300]
    }, {
        color: '#B77C3D',
        name: 'Year 2024',
        data: [814, 841, 600, 400, 550, 680]
    }]
};

const Card1 = () => {
    return (
        <>
            <div className='card-header'>
                <div className='header-left'>
                    Revenue
                </div>
                <div className='header-right'>
                </div>
            </div>
            <div>
                <HighchartsReact
                    highcharts={Highcharts}
                    options={chartData}
                />
            </div>
        </>
    )
}

const Card2 = () => {
    return (
        <>
            <div className='card-header'>
                <div className='header-left'>
                    Customers
                </div>
                <div className='header-right'>
                    <Select
                        defaultValue="1"
                        style={{ width: 120 }}
                        options={[
                            { value: '1', label: 'Customers' },
                            { value: '2', label: 'Taks' },
                            { value: '3', label: 'Orders' },
                        ]}
                    />
                </div>
            </div>
            <div>
                <HighchartsReact
                    highcharts={Highcharts}
                    options={chartData}
                />
            </div>
        </>
    )
}

const cardList = [
    {
        id: "card1",
        name: <Card1 />,
    },
    {
        id: "card2",
        name: <Card2 />,
    },
];

const Dashboard = () => {
    const ref1 = useRef(null);
    const ref2 = useRef(null);
    const ref3 = useRef(null);
    const ref4 = useRef(null);

    const [openMenu, setOpenMenu] = useState(false);

    const showDrawer = () => {
        setOpenMenu(true);
    };

    const onCloseDrawer = () => {
        setOpenMenu(false);
    };

    const [open, setOpen] = useState(true);

    const onClose = () => {
        localStorage.setItem("seenPopUp", true);
        setOpen(false);
    };

    useEffect(() => {
        let userreturn = localStorage.getItem("seenPopUp");
        setOpen(!userreturn);
    }, []);

    const steps = [
        {
            title: 'Theme Change',
            description: 'Change your theme as Light Or Dark.',
            target: () => ref1.current,
            cover: (
                <Button type='primary' ghost className="skip-btn2" onClick={onClose}>
                    Skip
                </Button>
            )
        },
        {
            title: 'Customize Cards Positions',
            description: 'Darg and Drop to Customize Positions Of Statics',
            cover: (
                <>
                    <img
                        alt="tour.png"
                        src={dargCardImg}
                    />
                    <Button type='primary' className="skip-btn2" onClick={onClose}>
                        Skip
                    </Button>
                </>
            ),
            target: () => ref2.current,
        },
        {
            title: 'Change Menu Position',
            description: 'Darg and Drop to change Menu Positions/',
            cover: (
                <>
                    <img
                        alt="tour.png"
                        src={dargMenuImg}
                    />
                    <Button type='primary' className="skip-btn2" onClick={onClose}>
                        Skip
                    </Button>
                </>
            ),
            target: () => ref3.current,
        },
        {
            title: 'Chat With Luxare AI',
            description: 'You can take some help if you need.',
            target: () => ref4.current,
            cover: (
                <Button type='primary' className="skip-btn4" onClick={onClose}>
                    Finish
                </Button>
            )
        },
    ];

    const [characters, updateCharacters] = useState(menuList);

    function handleOnDragEnd(result) {
        if (!result.destination) return;

        const items = Array.from(characters);
        const [reorderedItem] = items.splice(result.source.index, 1);
        items.splice(result.destination.index, 0, reorderedItem);

        updateCharacters(items);
    }

    const [characters2, updateCharacters2] = useState(cardList);

    function handleOnDragEnd2(result) {
        if (!result.destination) return;

        const items = Array.from(characters2);
        const [reorderedItem] = items.splice(result.source.index, 1);
        items.splice(result.destination.index, 0, reorderedItem);

        updateCharacters2(items);
    }

    return (
        <div className='main-wrapper'>
            <CrmChat />
            <div className='dummychat-tour' ref={ref4}></div>
            <Tour open={open} onClose={() => setOpen(false)} steps={steps} />
            <div className='dummylight-dark' ref={ref1}></div>
            <MediaQuery minWidth={991}>
                <div className='leftbar'>
                    <div className='logo'>
                        <img className='logo-light' src={logolight} alt='' />
                    </div>
                    <div ref={ref3}>
                        <Menus />
                    </div>
                    <Link className='logout-btn' to='/'><SvgIcon name='logout' viewbox="0 0 321 290" /> Logout</Link>
                </div>
            </MediaQuery>
            <MediaQuery maxWidth={991}>
                <Button className='menu-btn' type="primary" onClick={showDrawer}>
                    <SvgIcon name="menu" viewbox="0 0 46 32" />
                </Button>
                <Drawer placement='left' width={280} className='menu-drawer' title={null} closeIcon={false} onClose={onCloseDrawer} open={openMenu}>
                    <div className='leftbar'>
                        <div className='logo'>
                            <img className='logo-light' src={logolight} alt='' />
                        </div>
                        <div ref={ref3}>
                            <Menus />
                        </div>
                        <Link className='logout-btn' to='/'><SvgIcon name='logout' viewbox="0 0 321 290" /> Logout</Link>
                    </div>
                </Drawer>
            </MediaQuery>
            <div className='right-wrapper'>
                <div className='dashboard-wrapper'>
                    <div className='main-title'>Dashboard <p>Last Login Monday 26 Feb, 2024</p></div>
                    <div ref={ref2}>
                        <DragDropContext onDragEnd={handleOnDragEnd}>
                            <Droppable droppableId="widget-list">
                                {(provided) => (
                                    <div className="widget-list" {...provided.droppableProps} ref={provided.innerRef}>
                                        {characters.map(({ id, name, icon, counts, viewbox }, index) => {
                                            return (
                                                <Draggable key={id} draggableId={id} index={index}>
                                                    {(provided) => (
                                                        <div className='widget-card' ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps}>
                                                            <div className='card-header'>
                                                                <div className='icons'>
                                                                    <SvgIcon name={icon} viewbox={viewbox} />
                                                                </div>
                                                                {name}
                                                            </div>
                                                            <div className='card-bottom'>
                                                                <h2>{counts}</h2>
                                                            </div>
                                                        </div>
                                                    )}
                                                </Draggable>
                                            );
                                        })}
                                        {provided.placeholder}
                                    </div>
                                )}
                            </Droppable>
                        </DragDropContext>
                    </div>
                    <div>
                        <DragDropContext onDragEnd={handleOnDragEnd2}>
                            <Droppable droppableId="dashboardbottom">
                                {(provided) => (
                                    <div className="dashboard-bottom" {...provided.droppableProps} ref={provided.innerRef}>
                                        {characters2.map(({ id, name }, index) => {
                                            return (
                                                <Draggable key={id} draggableId={id} index={index}>
                                                    {(provided) => (
                                                        <div className='card-left' ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps}>
                                                            {name}
                                                        </div>
                                                    )}
                                                </Draggable>
                                            );
                                        })}
                                        {provided.placeholder}
                                    </div>
                                )}
                            </Droppable>
                        </DragDropContext>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Dashboard;